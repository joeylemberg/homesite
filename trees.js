	var ctx;
	var ThanksTree = {
		init: function(){
			var txt;
			ctx = document.getElementById('thank_you_tree').getContext('2d');
			ctx.beginPath();
			ctx.lineWidth = 1;
			ctx.fillStyle = 'black';
			ctx.strokeStyle = 'rgb(200,200,200)';
			ctx.font = "bold 15pt Tahoma";
			txt = 'Math.Random Canvas Trees';
			ctx.fillText(txt, 10, 30);
			ThanksTree.addBranch(470,320,-1.7,8);
			setInterval(ThanksTree.step, 20);
			ctx.beginPath();
			ctx.strokeStyle = 'rgb(39, ' + Math.round(100 + (100 * Math.random())) + ', 0)';
			ctx.fillStyle = 'rgba(' + Math.round(50 + (200 * Math.random())) + ', ' + Math.round(50 + (200 * Math.random())) + ', 0,50)';
		},
		step: function(){
			var branch;
			for(var i = 0; i < ThanksTree.branches.length; i++){
				branch = ThanksTree.branches[i];
				if(!ThanksTree.leavesOn){
				branch.theta -= 0.1;
				branch.theta += 0.2 * Math.random();
			}
				ctx.beginPath();
				ctx.lineWidth = branch.width;
				ctx.moveTo(branch.x,branch.y);
				branch.x += Math.cos(branch.theta);
				branch.y += Math.sin(branch.theta);
				branch.length ++;
				ctx.lineTo(branch.x,branch.y);
				ctx.stroke();
				if(branch.length > branch.width * 5 + 1){
					if(branch.width > 1){
						if(ThanksTree.leavesOn){
							ctx.fillRect(branch.x -4, branch.y -4, 8,8);
						}
					ThanksTree.addBranch(branch.x,branch.y,branch.theta + Math.random() * 2 - 1, branch.width * 0.8);
					branch.width *= 0.8;
					branch.length = 0;
					if(!ThanksTree.leavesOn && branch.width < 2.5){
						ThanksTree.leavesOn = 1;
					}
					}else{
						ThanksTree.branches = [];
						ThanksTree.addBranch(50 + Math.random() * 800,320,-1.8 + Math.random()/2,4 + 8*Math.random());
						ctx.beginPath();
						ctx.strokeStyle = 'rgb(39, ' + Math.round(20 + (60 * Math.random())) + ', 0)';
						ctx.fillStyle = 'rgba(' + Math.round(50 + (200 * Math.random())) + ', ' + Math.round(50 + (200 * Math.random())) + ', 0,0.5)';
						ThanksTree.leavesOn = 0;
						break;
					}
				}
			}
		},
		addBranch: function(x,y,t,w){
			ThanksTree.branches.push({
				length: 0,
				x: x,
				y: y,
				theta: t,
				width: w
			});
		},
		leavesOn: 0,
		branches: []
	};

	window.onload = ThanksTree.init;